package model;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetSocketAddress;
import java.net.SocketException;

/**
 * 
 * @author Illum
 * @since 01-April-2014
 */
public class MessageTransmitter
{
	private DatagramSocket socket;
	private InetSocketAddress serverAddress;
	
	
	public MessageTransmitter(DatagramSocket socket, InetSocketAddress serverAddress)
	{
		this.serverAddress = serverAddress;
		this.socket = socket;
		sendPublicMessage("[Joined Chat]");
	}
	
	
	public void sendPublicMessage(String message)
	{
		byte[] messageBytes = ("MSGA " + message).getBytes();
		try
		{
			DatagramPacket publicMsg = new DatagramPacket(messageBytes, messageBytes.length, serverAddress);
			socket.send(publicMsg);
		}
		catch (SocketException e)
		{
			e.printStackTrace();
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}
	
	
	public void sendPrivateMessage(String receivingNick, String message)
	{
		// Wrapping nick in parentheses according to protocol
		byte[] messageBytes = ("MSGP " + "(" + receivingNick + ")" + " " + message).getBytes();
		try
		{
			DatagramPacket privateMsg = new DatagramPacket(messageBytes, messageBytes.length, serverAddress);
			socket.send(privateMsg);
		}
		catch (SocketException e)
		{
			// return fail to GUI?
			e.printStackTrace();
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}

	
}
