package model;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetSocketAddress;
import java.net.SocketException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Random;
import java.util.regex.Pattern;

import javafx.application.Platform;

import controller.ChatWindowController;
import controller.LoginWindowController;

/**
 * 
 * @author Illum
 * @since 01-April-2014
 */
public class Communicator extends Thread
{
	private DatagramSocket socket;
	private int localPort;
	private final int PACKETSIZE = 400;
	private Pattern whiteSpacePattern;
	private Pattern parenthesesWhiteSpacePattern;
	private DatagramPacket pongMsg;
	private InetSocketAddress serverAddress;
	private Calendar calendar;
	private SimpleDateFormat hourAndMin;
	
	private ChatWindowController chatController;
	private LoginWindowController loginController;

	
	public Communicator(InetSocketAddress serverAddress, String nick, LoginWindowController loginController)
	{
		calendar = Calendar.getInstance();
		hourAndMin = new SimpleDateFormat("HH:mm");
		this.serverAddress = serverAddress;
		localPort = 1099;	
		whiteSpacePattern = Pattern.compile("\\s+");
		parenthesesWhiteSpacePattern = Pattern.compile("\\){1}");
		try
		{
			inHonourOfTestingOnTheSameHostWhileAvoidingPortConflictsDatagramSocketShallBeSetUpLikeThis();
			byte[] pongBytes = "PONG".getBytes();
			pongMsg = new DatagramPacket(pongBytes, pongBytes.length, serverAddress);
			this.loginController = loginController;
			byte[] joinBytes = ("JOIN" + " " + "(" + nick + ")").getBytes();
			DatagramPacket requestJoin = new DatagramPacket(joinBytes, joinBytes.length, serverAddress);
			socket.send(requestJoin);
		}
		catch (SocketException e)
		{
//			run joinErr() here as well?
			e.printStackTrace();
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}
	
	
	@Override
	public void run()
	{
		while (true)
		{
			receive();
		}
	}
	

	private void receive()
	{
		DatagramPacket packetFromServer = new DatagramPacket(new byte[PACKETSIZE], PACKETSIZE);
		try
		{
			socket.receive(packetFromServer);
			String incMessage = new String(packetFromServer.getData()).trim();
			//System.out.println("Raw trimmed message: [" + incMessage + "]");
			String[] splitMessage = whiteSpacePattern.split(incMessage, 2);	// Up to 2 strings around the split
			handleMessage(splitMessage);
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		
	}
	

	private void handleMessage(final String[] splitMessage) throws IOException
	{
		// This seems to be mandatory when working in a javaFx thread
		Platform.runLater(new Runnable ()
		{
			@Override
			public void run()
			{
				if (splitMessage != null)
				{
					try{
						switch(splitMessage[0])
						{
						case "PING"	:	ping(); break;
						case "MSG"	:	incMsg(parenthesesWhiteSpacePattern.split(splitMessage[1], 2)); break;
						case "JOK"	:	joinSuc(); break;
						case "JERR"	: joinErr(); break;
						default : System.out.println("Invalid request: " + splitMessage[0]);
						}
					}
					catch (IOException e)
					{
						e.printStackTrace();
					}
				}
			}
		});
	}
	
	
	private void joinErr()
	{
		loginController.joinError();
	}
	

	private void joinSuc() throws IOException
	{
		loginController.joinSucces(new MessageTransmitter(socket, serverAddress));
	}
	

	private void incMsg(String[] nameAndMsg)
	{
		if (nameAndMsg.length > 1)	// array contains both name and message
		{
			chatController.putMsgOnScreen(clock() + " " + nameAndMsg[0].substring(1, nameAndMsg[0].length()) + ":   " + nameAndMsg[1]);
		}
	}
	

	private void ping() throws IOException
	{
		socket.send(pongMsg);
	}
	
	private String clock()
	{
		return hourAndMin.format(calendar.getTime());
	}

	private void inHonourOfTestingOnTheSameHostWhileAvoidingPortConflictsDatagramSocketShallBeSetUpLikeThis() throws SocketException
	{
		int openSocketTries = 0;
		while (true)
		{
			try
			{
				socket = new DatagramSocket(localPort);
				break;	// Succes!
			}
			catch (SocketException e)
			{
				localPort = localPort + new Random().nextInt(100) + 1;	// If failing to create socket, increment port with 1-100
				if (++openSocketTries == 10)	// 10 tries have been made. Give up man.
				{
					throw e;
				}
			}
		}
	}
	

	public void setChatController(ChatWindowController chatController)
	{
		this.chatController = chatController;
	}
	

	public void setLoginController(LoginWindowController loginController)
	{
		this.loginController = loginController;
	}

	
}