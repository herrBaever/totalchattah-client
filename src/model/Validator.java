package model;

import java.util.regex.Pattern;


/**
 * 
 * @author Illum
 * @since 01-April-2014
 * Lovely reading : http://docs.oracle.com/javase/tutorial/essential/regex/index.html
 */
public class Validator
{
//	private static final String ohMyGodIPRegex = "\\b((25[0-5]|2[0-4][0-9]|[0-1]{0,1}[0-9]{0,1}[0-9])(\\Q.\\E25[0-5]|2[0-4][0-9]|[0-1]{0,1}[0-9]{0,1}[0-9]){3})\\b";
//	private static final String ohMyGodIPRegex = "^((25[0-5]|2[0-4][0-9]|[0-1]{0,1}[0-9]{0,1}[0-9])(\\Q.\\E25[0-5]|2[0-4][0-9]|[0-1]{0,1}[0-9]{0,1}[0-9]){3})$";
//	private static final String ohMyGodIPRegex = "^((25[0-5]|2[0-4][0-9]|[0-1]{0,1}[0-9]{0,1}[0-9])(\\Q.\\E25[0-5]|2[0-4][0-9]|[0-1]{0,1}?[0-9]{0,1}?[0-9]){3})$";
//	private static final String ohMyGodIPRegex = "^((25[0-5]|2[0-4][0-9]|[0-1]{0,1}[0-9]{0,1}[0-9])(\\Q.\\E25[0-5]|2[0-4][0-9]|[0-1]{0,1}?[0-9]{0,1}?[0-9]){3})$";
	private static final String ohMyGodIPRegex = "^(25[0-5]|2[0-4][0-9]|[0-1]{0,1}[0-9]{0,1}[0-9])(\\Q.\\E(25[0-5]|2[0-4][0-9]|[0-1]{0,1}[0-9]{0,1}[0-9])){3}$";
	private static Pattern ipPattern = Pattern.compile(ohMyGodIPRegex);
	
//	private static final String nickRegex = "^[a-�A-�0-9]+([ _\\-][a-�A-�0-9]+)*$";
//	private static final String nickRegex = "^[a-�A-�0-9]{1,}([ _\\-][a-�A-�0-9]{1,}){0,}$";
	private static final String nickRegex = "^[a-�����A-�����0-9]{1,}([ _\\-][a-�����A-�����0-9]{1,}){0,}$";
	private static Pattern nickPattern = Pattern.compile(nickRegex);
	

	
	public static boolean isValidIPv4(String ip)
	{
		return ipPattern.matcher(ip).matches();
	}
	
	
	public static boolean isValidNick(String nick)
	{
		return nickPattern.matcher(nick).matches();
	}
	
	
	public static boolean isValidPort(String port)
	{
		Integer validationInt;
		try
		{
			validationInt = Integer.parseInt(port);
		}
		catch (NumberFormatException e)
		{
			return false;
		}
		return (validationInt > -1 && validationInt <= 65536);
	}
	
	
}
