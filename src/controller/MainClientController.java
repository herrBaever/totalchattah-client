package controller;

import java.io.IOException;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

/**
 * 
 * @author Illum
 * @since 01-April-2014
 */
public class MainClientController extends Application
{
	
	private AnchorPane loginPane;
	private FXMLLoader loader;
	private static Stage primaryStage;
	private Scene loginScene;

	
	@Override
	public void start(Stage primaryStage) throws IOException 
	{
		MainClientController.primaryStage = primaryStage;
		MainClientController.primaryStage.setTitle("TotalChattah");
		MainClientController.primaryStage.setWidth(206);
		MainClientController.primaryStage.setHeight(298);
		
		loader = new FXMLLoader(MainClientController.class.getResource("/view/LoginView.fxml"));
		loginPane = (AnchorPane) loader.load();
		loginScene = new Scene(loginPane);
		primaryStage.setScene(loginScene);
		primaryStage.show();
		primaryStage.setResizable(false);
	}

	
	public static void main(String[] args) 
	{
		launch(args);
	}
	
	
	public static Stage getPrimaryStage() 
	{
		return primaryStage;
	}
	
	
}
