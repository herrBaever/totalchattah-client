package controller;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.Timer;
import java.util.TimerTask;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import model.Communicator;
import model.MessageTransmitter;
import model.Validator;

/**
 * 
 * @author Illum
 * @since 01-April-2014
 */
public class LoginWindowController
{
	
	
   @FXML
   private TextField TextFieldServerIP;
   @FXML
   private TextField TextFieldServerPort;
   @FXML
   private TextField TextFieldNick;
   @FXML
   private Label errorLabel;
   @FXML
   private Button joinButton;
   @FXML
   private ProgressIndicator joinProcessIndi;
   
   private Timer timer;
   private Communicator communicator;
	
   
	@FXML
	public void fireJoinServer(ActionEvent event)
	{
		String nick = TextFieldNick.getText();
		String serverPort = TextFieldServerPort.getText();
		String serverIp = TextFieldServerIP.getText();
		if (!Validator.isValidNick(nick))
		{
			errorLabel.setText("Invalid Nick");
		}
		else if (!Validator.isValidPort(serverPort))
		{
			errorLabel.setText("Invalid port");
		}
		else if (!Validator.isValidIPv4(serverIp))
		{
			errorLabel.setText("Invalid IP");
		}
		// Everything should be ok by now
		else
		{
			errorLabel.setText("");
			InetSocketAddress serverAddress = new InetSocketAddress(serverIp, new Integer(serverPort));
			communicator = new Communicator(serverAddress, nick, this);
			communicator.start();
			// When server isn't responding immediately
			disableControl(true);
			joinProcessIndi.setVisible(true);
			timer = new Timer();
			timer.schedule(new TimerTask()
								{
									@Override
									public void run()
									{
										// For the sake of the javaFx thread
										Platform.runLater(new Runnable()
										{
											@Override
											public void run()
											{
												disableControl(false);
												joinProcessIndi.setVisible(false);
												errorLabel.setText("No response from server");			
											}		
										});
									}					
								}, 3000);	// Delay this task 3000 milliseconds
		}
	}

	
	public void joinError()
	{
		errorLabel.setText("Choose another name");
	}
	
	
	public void joinSucces(MessageTransmitter msgTrsm) throws IOException
	{
		FXMLLoader chatFXMLLoader = new FXMLLoader(MainClientController.class.getResource("/view/ChatView.fxml"));
		AnchorPane chatPane = (AnchorPane) chatFXMLLoader.load();
		ChatWindowController chatContr = chatFXMLLoader.getController();
		communicator.setChatController(chatContr);
		chatContr.setMsgTrnsm(msgTrsm);
		Scene chatScene = new Scene(chatPane);
		MainClientController.getPrimaryStage().setWidth(240);
		MainClientController.getPrimaryStage().setScene(chatScene);
	}

	
	private void disableControl(boolean flag)
	{
		TextFieldNick.setDisable(flag);
		TextFieldServerIP.setDisable(flag);
		TextFieldServerPort.setDisable(flag);
		joinButton.setDisable(flag);
	}
	
	
}
