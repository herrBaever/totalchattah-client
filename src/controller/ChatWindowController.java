package controller;

import javafx.fxml.FXML;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleButton;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;

import model.MessageTransmitter;

/**
 * 
 * @author Illum
 * @since 01-April-2014
 */
public class ChatWindowController
{
	@FXML
	private TextArea chatMsgArea;
	@FXML
	private TextField msgField;
	@FXML
	private TextField privateMsgReceiver;
	@FXML
	private ToggleButton togglePrivateButton;

	private MessageTransmitter msgTrnsm;
	
	
	@FXML
	public void fireSendMsg()
	{
		if (!msgField.getText().isEmpty())
		{
			if (togglePrivateButton.isSelected() && !privateMsgReceiver.getText().isEmpty())
			{
				msgTrnsm.sendPrivateMessage(privateMsgReceiver.getText(), msgField.getText());
				msgField.clear();
			}
			else if (!togglePrivateButton.isSelected())
			{
				msgTrnsm.sendPublicMessage(msgField.getText());
				msgField.clear();
			}
		}
	}


	@FXML
	public void fireTogglePrivateMsg() 
	{
		privateMsgReceiver.setVisible(togglePrivateButton.isSelected());
		if (togglePrivateButton.isSelected())
		{
			privateMsgReceiver.requestFocus();
		}
		else
		{
			privateMsgReceiver.clear();
		}
	}

	
   @FXML
   public void handlerEnterPressed(KeyEvent event) 
   {
   	if (event.getCode() == KeyCode.ENTER)
   	{
   		fireSendMsg();
   	}
   }
   
	
	public void putMsgOnScreen(String nameAndMsg)
	{
		chatMsgArea.appendText("\n" + nameAndMsg);
	}


	public void setMsgTrnsm(MessageTransmitter msgTrnsm)
	{
		this.msgTrnsm = msgTrnsm;
	}

	
}
